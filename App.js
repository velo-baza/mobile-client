import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainMenu from './src/components/MainMenu/MainMenu';
import ListOfPoints from './src/components/Points/List/List';


export default class App extends Component {
  render () {
    const Stack = createStackNavigator();
    
    return (
      // todo add async animation preview while load user details, maps details etc...
      <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen
              name="Main menu"
              component={MainMenu}
              options={{ title: 'Menu' }}
            />
            <Stack.Screen
              name="List of points"
              component={ListOfPoints}
              options={{ title: 'Points' }}
            />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
