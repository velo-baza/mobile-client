import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      color: 'rgb(59,108,212)',
      fontSize: 42,
      fontWeight: '100',
      textAlign: 'center',
    },
});

export {styles}
