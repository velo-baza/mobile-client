import React, { Component } from "react";
import { styles } from "./styles";
import { View, Text } from "react-native";

import { withTranslation } from 'react-i18next';

class MainMenu extends Component {
    render () {
        return (
            <View style={styles.container}>
                <Text>{this.props.t('menu')}</Text>
            </View>
        );
    }
};

export default withTranslation() (MainMenu);